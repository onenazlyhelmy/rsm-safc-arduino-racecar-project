#include <SPI.h>
#include <Wire.h>
#include <Adafruit_GFX.h>
#include <Adafruit_SSD1306.h>
///...let race motherf*****r.......
#define SCREEN_WIDTH 128 
#define SCREEN_HEIGHT 64 
//---yo bitch!!!!!!!
// Declaration for an SSD1306 display connected to I2C (SDA, SCL pins)
// The pins for I2C are defined by the Wire-library. 
// On an arduino UNO:       A4(SDA), A5(SCL)
// On an arduino MEGA 2560: 20(SDA), 21(SCL)
// On an arduino LEONARDO:   2(SDA),  3(SCL), ...
#define OLED_RESET     -1 
#define SCREEN_ADDRESS 0x3C // bro..ni address refer kt manufacturer...
Adafruit_SSD1306 display(SCREEN_WIDTH, SCREEN_HEIGHT, &Wire, OLED_RESET);

#define NUMFLAKES     10 // Number of snowflakes in the animation example

#define LOGO_HEIGHT   16
#define LOGO_WIDTH    16
static const unsigned char PROGMEM logo_bmp[] =
{ 0b00000000, 0b11000000,
  0b00000001, 0b11000000,
  0b00000001, 0b11000000,
  0b00000011, 0b11100000,
  0b11110011, 0b11100000,
  0b11111110, 0b11111000,
  0b01111110, 0b11111111,
  0b00110011, 0b10011111,
  0b00011111, 0b11111100,
  0b00001101, 0b01110000,
  0b00011011, 0b10100000,
  0b00111111, 0b11100000,
  0b00111111, 0b11110000,
  0b01111100, 0b11110000,
  0b01110000, 0b01110000,
  0b00000000, 0b00110000 };
 

#define SIGNAL_PIN A0 // voltage sensor

int out_to_map_pin = 5;  // voltage write pin assigned       
int brightness = 0;  // how bright the LED is
int fadeAmount = 5;  // how many points to fade the LED by
 
// Floats for ADC voltage & Input voltage
float adc_voltage = 0.0;
float in_voltage = 0.0;
float out_ecu_decimal = 0.0;
float out_ecu_volt_val = 0.0;
 
// Floats for resistor values in divider (in ohms)
float R1 = 30000.0;
float R2 = 7500.0;
 
// Float for Reference Voltage
float ref_voltage = 5.0;
 
// Integer for ADC value
int adc_value = 0;
 static char outstr[15];
  static char outstrmap[15];
  static char outstrmapV[15];

 
void setup()
{
  // Setup Serial Monitor
  pinMode(out_to_map_pin, OUTPUT);
  Serial.begin(9600);
  Serial.println ("Voltage sensor Test"); 
 // SSD1306_SWITCHCAPVCC = generate display voltage from 3.3V internally
  if(!display.begin(SSD1306_SWITCHCAPVCC, SCREEN_ADDRESS)) {
    Serial.println(F("SSD1306 allocation failed"));
    for(;;); // Don't proceed, loop forever
   }
    display.display();
      delay(2000);
      display.clearDisplay();
        display.drawPixel(10, 10, SSD1306_WHITE);

  // Show the display buffer on the screen. You MUST call display() after
  // drawing commands to make them visible on screen!
  display.display();
  delay(2000);

}


void wrt_value(char *str) {
  
  display.clearDisplay();

  display.setTextSize(2);             
  display.setTextColor(SSD1306_WHITE);     
  display.setCursor(0,0);           
  display.println(str);

 display.setTextSize(1); 
  display.setTextColor(SSD1306_WHITE); 
  
   
//  display.setCursor(0,40);  
//  display.println("drac-nazly");
//
//  display.setTextSize(2);          
//  display.setTextColor(SSD1306_WHITE);
//  display.print(F("0x")); display.println(0xDEADBEEF, HEX);

  display.display();
  delay(1000);
}


void wrt_value_mapecu(char *str,char *str_ecu,char *str_ecu_v) {
  
  display.clearDisplay();

  display.setTextSize(1.5);             // Normal 1:1 pixel scale
  display.setTextColor(SSD1306_WHITE);        // Draw white text
  display.setCursor(0,0);  
  char buf[5];
  strcpy(buf,"Rd(v):");
  strcat(buf,str);
  display.println(buf);

  display.setCursor(0,10); 
  char buff[5];
  strcpy(buff,"Wr(decimal):");
  strcat(buff,str_ecu);
  display.println(buff);

  display.setCursor(0,20); 
  char buffe[5];
  strcpy(buffe,"Wr(volt):");
  strcat(buffe,str_ecu_v);
  display.println(buffe);

  display.setTextSize(1);
  display.setCursor(0,40);  
  display.println("RSM MapSensor-turbo");
  display.setCursor(0,50);  
  display.println("drac-nazly");
//
//  display.setTextSize(2);             // Draw 2X-scale text
//  display.setTextColor(SSD1306_WHITE);
//  display.print(F("0x")); display.println(0xDEADBEEF, HEX);

  display.display();
  delay(1000);
}

void loop() {
   

  adc_value = analogRead(SIGNAL_PIN); // read from sensor
 
  // Determine voltage at ADC input
  adc_voltage  = (adc_value * ref_voltage) / 1024.0;
 
  // Calculate voltage at divider input
  in_voltage = adc_voltage / (R2 / (R1 + R2)) ;

  out_ecu_decimal = 255 * in_voltage / 5;// actually its decimal value,for voltage value,need to calculate..voltage tune!!!

  out_ecu_volt_val  = ((out_ecu_decimal * ref_voltage) / 1024.0) / (R2 / (R1 + R2-7300));///voltage real write value...for reading only

  analogWrite(out_to_map_pin, out_ecu_decimal); //write to pin 5/map-ecu
  dtostrf(out_ecu_decimal,4, 3, outstrmap);
  //wrt_value_mapecu(outstrmap);// write voltage map ecu to OLED
 
  // Print results to Serial Monitor to 2 decimal places
  Serial.print("Input Voltage ==> ");
  Serial.println(in_voltage, 2);
   Serial.print("Output decimal pin ==> ");
  Serial.println(out_ecu_decimal, 2);

  Serial.print("Output Voltage(human reading)==> ");
  Serial.println(out_ecu_volt_val, 2);

  dtostrf(in_voltage,4, 3, outstr);
   dtostrf(out_ecu_volt_val,4, 3, outstrmapV);
  //wrt_value(outstr); // write sensor voltage to OLED
  wrt_value_mapecu(outstr,outstrmap,outstrmapV);

  Serial.println(" ");

   delay(40);
}
